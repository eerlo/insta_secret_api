#-*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

class InstagramProfile(models.Model):
    """
    Represents a instagram profile
    """
    id = models.CharField(verbose_name='Profile Identifier(name)',
                          max_length=200,
                          primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = [u'created_at']

    def __unicode__(self):
        return self.id

    def get_absolute_instagram_url(self):
        return u'https://www.instagram.com/%s/' % self.id


class ProfileSubscription(models.Model):
    """
    Users subscriptions of profiles
    """

    profile = models.ForeignKey(InstagramProfile,
                                related_name='subscriptions')
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = [u'profile']

    def __unicode__(self):
        return u'%s -> %s' % (self.user.username, self.profile)


class ProfileContent(models.Model):
    """
    A content of a profile
    """
    profile = models.ForeignKey(InstagramProfile,
                                related_name='contents')
    url = models.URLField()
    description = models.TextField(null=True,
                                   blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
