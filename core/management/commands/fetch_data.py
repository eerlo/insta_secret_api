#-*- coding: utf-8 -*-


import time
from selenium import webdriver

from django.core.management.base import BaseCommand, CommandError

from core.models import InstagramProfile

class Command(BaseCommand):
    help = 'Fetch the new data from the instagram website'

    def add_arguments(self, parser):
        #parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        browser_instance = webdriver.Firefox()

        for profile in InstagramProfile.objects.all():
            self.stdout.write(
                self.style.NOTICE('Entering profile "%s"' % profile.id)
            )
            browser_instance.get(profile.get_absolute_instagram_url())
        
            images = browser_instance.find_elements_by_css_selector(
                         'article div div>a>div>div>img'
                     )
            first_page_contents = [{'src': i.get_attribute('src'),
                                    'title': i.get_attribute('title')} \
                                 for i in images]
            already_exists = profile.contents.filter(
                                 url__in=[i['src'] for i in first_page_contents]
                             ).values_list(u'url', flat=True)
            new_contents = [i for i in first_page_contents \
                            if i['src'] not in already_exists]
            self.stdout.write(
                self.style.NOTICE(
                    '    there are %s new contents' % len(new_contents)
                )
            )
            for num, content in enumerate(new_contents):
                self.stdout.write(
                    self.style.NOTICE(
                        '        creating %s of %s' % (num, len(new_contents))
                    )
                )
                profile.contents.create(url=content['src'],
                                        description=content['title'])
        browser_instance.close()
